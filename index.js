'use strict';

const _ = require('lodash');
const co = require('co');
const hash = require('object-hash');
const Promise = require('bluebird');
Promise.config({ cancellation: true });

/**
 * @typedef   CreateTableResult
 * @type      Object
 * @property  {String}              tableName                 The table name.
 * @property  {Boolean}             success                   A boolean value indicating whether the creation was successfully done.
 */

/**
 * @typedef   SQResult
 * @type      Object
 * @property  {Object[]}            items                     Array of items.
 * @property  {Object}              [nextKey]                 The next key if the operation hasn't finished.
 * @property  {Boolean}             done                      A boolean value indicating whether the operation has done.
 */

/**
 * @callback  ScanCallback
 * @param     {SQResult}            params                    The callback parameters.
 * @return    {Promise}                                       A Promise that is resolved when the callback is done and ready for next batch.
 */

/**
 * @callback  QueryCallback
 * @param     {SQResult}            params                    The callback parameters.
 * @return    {Promise}                                       A Promise that is resolved when the callback is done and ready for next batch.
 */

/**
 * @typedef   Condition
 * @type      Object
 * @property  {String}              expression                The expression.
 * @property  {Object}              attributeNames            Object that maps attribute name placeholder to the attribute name.
 * @property  {Object}              attributeValues           Object that maps attribute value placeholder to the attribute value.
 */

// conditions will be like { a: 'test', b: 1234 } or [{ expression: '#b > :b', attributeNames: { '#b': 'b' }, attributeValues: { ':b': 1234 } }, ... ]

module.exports = ({ aws = require('aws-sdk'), logger, region, accessKeyId, secretAccessKey } = {}) => {
  const dynamoDB = new aws.DynamoDB({
    accessKeyId,
    secretAccessKey,
    region
  });
  const docClient = new aws.DynamoDB.DocumentClient({
    service: dynamoDB
  });
  const capacity = {};

  logger = _.defaults(logger, {
    trace: _.partial(console.log, '[TRACE]'),
    debug: _.partial(console.log, '[DEBUG]'),
    info: _.partial(console.info, '[INFO]'),
    warn: _.partial(console.warn, '[WARN]'),
    error: _.partial(console.error, '[ERROR]'),
  });

  /**
   * The class provides some basic functionalities of DynamoDB operations.
   * 
   * @class DynamoDBUtils
   */

  class DynamoDBUtils {
    static get dynamoDB() {
      return dynamoDB;
    }

    static get docClient() {
      return docClient;
    }

    /**
     * Create tables.
     * @static
     * @param   {TableConfig[]}   tableConfigurations             Array of table configurations that will be forwarded to DynamoDB.createTable().
     * @param   {Object}          [options]                       Options.
     * @param   {Boolean}         [options.waitForDone=true]      A boolean value indicating whether to wait for the creation finished.
     * @param   {Number}          [options.concurrency=3]         Number of concurrency.
     * @return  {Promise<CreateTableResult>}                      A Promise that resolves an array of results.
     * @memberof DynamoDBUtils
     */
    static createTables(tableConfigurations, { waitForDone = true, concurrency = 3 } = {}) {
      const CREATE_TABLE_SUPPORTED_PROPERTIES = ['TableName', 'AttributeDefinitions', 'KeySchema', 'ProvisionedThroughput', 'GlobalSecondaryIndexes', 'LocalSecondaryIndexes', 'StreamSpecification'];
      const UPDATE_TTL_SUPPORTED_PROPERTIES = ['TableName', 'TimeToLiveSpecification'];

      return Promise.resolve(co(function* handler() {

        const listTableResult = yield dynamoDB.listTables({}).promise();
        if (!_.isObject(listTableResult) || !_.isArray(listTableResult.TableNames)) {
          throw new Error('Invalid result of listTables().');
        }

        const { TableNames: tableNames } = listTableResult;
        logger.info('Current tables:', tableNames);

        return yield Promise.map(tableConfigurations, tableConfiguration => co(function* createTable() {
          const { TableName: tableName } = tableConfiguration;
          try {
            const createTableParams = _.pick(tableConfiguration, CREATE_TABLE_SUPPORTED_PROPERTIES);
            const updateTTLParams = _.pick(tableConfiguration, UPDATE_TTL_SUPPORTED_PROPERTIES);
            const toCreateTable = _.intersection(_.keys(tableConfiguration), CREATE_TABLE_SUPPORTED_PROPERTIES).length > 1;
            const toUpdateTTL = _.intersection(_.keys(tableConfiguration), UPDATE_TTL_SUPPORTED_PROPERTIES).length > 1;

            if (toCreateTable) {
              if (!_.includes(tableNames, tableName)) {
                const createTableResult = yield dynamoDB.createTable(createTableParams).promise();
                logger.info(`Create '${tableName}' successfully:\r\n${stringify(createTableResult)}`);
              } else {
                logger.info(`Table '${tableName}' already exists.`);
                const describeTableResult = yield dynamoDB.describeTable({ TableName: tableName }).promise();
                if (!_.isObject(describeTableResult) || !_.isObject(describeTableResult.Table)) {
                  throw new Error('Invalid result of describeTable().');
                }

                const result = isToUpdateTable(describeTableResult, createTableParams);
                if (_.isArray(result)) {
                  logger.info(`Updating table '${tableName}'...`);
                  yield dynamoDB.updateTable(buildUpdateTableParams(describeTableResult, createTableParams, result)).promise();
                } else {
                  logger.info(`All properties of '${tableName}' are the same, no need to update.`);
                }
              }
            }

            if (waitForDone || toUpdateTTL) {
              yield this.waitFor(tableName);
            }

            if (toUpdateTTL) {
              const describeTTLResult = yield dynamoDB.describeTimeToLive({ TableName: tableName }).promise();
              if (!_.isObject(describeTTLResult) || !_.isObject(describeTTLResult.TimeToLiveDescription)) {
                throw new Error('Invalid result of describeTimeToLive().');
              }

              const currentSpec = describeTTLResult.TimeToLiveDescription;
              const newSpec = updateTTLParams.TimeToLiveSpecification;

              const enabled = _.includes(['ENABLING', 'ENABLED'], currentSpec.TimeToLiveStatus);
              if (enabled !== newSpec.Enabled || currentSpec.AttributeName !== newSpec.AttributeName) {
                logger.info(`Updating time to live of '${tableName}'...`);
                yield dynamoDB.updateTimeToLive(updateTTLParams).promise();
              }
            }

            return {
              tableName,
              success: true
            };
          } catch (error) {
            logger.error(`Failed creating/updating '${tableName}':`, error);
            return {
              tableName,
              success: false
            };
          }
        }.bind(this)), { concurrency });
      }.bind(this)));
    }

    /**
     * Delete tables.
     * @static
     * @param   {String[]}        tableNames                      Array of table names to be deleted.
     * @param   {Object}          [options]                       Options.
     * @param   {Boolean}         [options.waitForDone=true]      A boolean value indicating whether to wait for the creation finished.
     * @param   {Number}          [options.concurrency=3]         Number of concurrency.
     * @return  {Promise}                                         A Promise that will be resolved when the operations was done if waitForDone is true,
     *                                                            or resolved immediately when the DynamoDB.deleteTable() was returned otherwise.
     * @todo    Return the deleted status of table.
     * @memberof DynamoDBUtils
     */
    static deleteTables(tableNames, { waitForDone = true, concurrency = 3 } = {}) {
      return Promise.resolve(co(function* handler() {
        const listTableResult = yield dynamoDB.listTables({}).promise();
        if (!_.isObject(listTableResult) || !_.isArray(listTableResult.TableNames)) {
          throw new Error('Invalid result.');
        }

        logger.info('Current tables:', listTableResult.TableNames);

        // only delete those existing ones
        tableNames = _.intersection(tableNames, listTableResult.TableNames);

        yield Promise.map(tableNames, tableName => co(function* deleteTable() {
          yield dynamoDB.deleteTable({
            TableName: tableName
          }).promise();

          if (waitForDone) {
            yield this.waitFor(tableName, { state: 'tableNotExists' });
          }
        }.bind(this)), { concurrency });
      }.bind(this)));
    }

    /**
     * List table names.
     * @static
     * @return  {Promise<String[]>}                               A Promise that resolves an array of table names.
     * @memberof DynamoDBUtils
     */
    static listTables() {
      return Promise.resolve(dynamoDB.listTables({}).promise())
        .then(result => _.get(result, 'TableNames', []));
    }

    /**
     * Wait for a specific state of the specified table.
     * @static
     * @param   {String}          tableName                       The table name.
     * @param   {Object}          [options]                       Options.
     * @param   {String}          [options.state='tableExists']   The state to be waited.
     * @return  {Promise}                                         A Promise that resolves the result from DynamoDB.waitFor().
     * @todo    The results should be simplified.
     * @memberof DynamoDBUtils
     */
    static waitFor(tableName, { state = 'tableExists' } = {}) {
      // check parameters
      if (!_.isString(tableName)) {
        logger.error(`Invalid table name: ${stringify(tableName)}`);
        return Promise.reject(new Error('Invalid parameters.'));
      }

      const waitForParams = {
        TableName: tableName
      };

      logger.debug('Waiting for table to be active...');

      return Promise.resolve(dynamoDB.waitFor(state, waitForParams).promise());
    }

    /**
     * Wait for the active status of the specified table.
     * @static
     * @param   {String}          tableName                       The table name.
     * @return  {Promise}                                         A Promise that resolves the result from DynamoDB.waitFor().
     * @todo    The results should be simplified.
     * @memberof DynamoDBUtils
     */
    static waitForActive(tableName, { timeout } = {}) {
      let promise = new Promise((resolve, reject, onCancel) => {
        let cancelled = false;
        if (onCancel) {
          onCancel(() => { cancelled = true; });
        }

        co(function* handler() {
          // eslint-disable-next-line no-unmodified-loop-condition
          while (!cancelled) {
            const { Table: tableResult } = yield this.waitFor(tableName, { status: 'tableExists' });
            const { TableStatus: status } = tableResult;
            if (status === 'ACTIVE') {
              break;
            }
            yield Promise.delay(1000);
          }
        }.bind(this)).then(resolve).catch(reject);
      });

      if (timeout) {
        promise = promise.timeout(timeout).catch(Promise.TimeoutError, (error) => {
          promise.cancel();
          return error;
        });
      }

      return promise;
    }

    /**
     * Adjust capacity of the specified table.
     * @static
     * @param   {String}          tableName                       The name of table.
     * @param   {Number}          [read=null]                     The read capacity.
     * @param   {Number}          [write=null]                    The write capacity.
     * @param   {Object}          [options]                       Options.
     * @param   {Boolean}         [options.restorable=true]       A boolean value indicating whether the adjustment is restorable.
     * @param   {Boolean}         [options.waitForDone=true]      A boolean value indicating whether to wait for the creation finished.
     * @return  {Promise}                                         A Promise that will be resolved when the operations was done if waitForDone is true,
     *                                                            or resolved immediately when the DynamoDB.updateTable() was returned otherwise.
     * @memberof DynamoDBUtils
     */
    static adjustCapacity(tableName, read, write, { restorable = true, waitForDone = true } = {}) {
      return Promise.resolve(co(function* handler() {
        // check parameters
        if (!_.isString(tableName) || ((!_.isInteger(read) || read < 1) && (!_.isInteger(write) || write < 1))) {
          throw new Error('Invalid parameters.');
        }

        // wait for the table to be ready
        yield this.waitFor(tableName);

        // get table info
        const describeTableParams = {
          TableName: tableName
        };
        const describeTableResult = yield dynamoDB.describeTable(describeTableParams).promise();
        logger.debug(`describeTable() result:\r\n${stringify(describeTableResult)}`);

        // get provisioned throughput
        const provisionedThroughput = _.get(describeTableResult, 'Table.ProvisionedThroughput');
        if (!_.isPlainObject(provisionedThroughput)) {
          logger.error(`Invalid result:\r\n${stringify(describeTableResult)}`);
          throw new Error('Invalid result.');
        }

        // get current read/write
        const currentRead = provisionedThroughput.ReadCapacityUnits;
        const currentWrite = provisionedThroughput.WriteCapacityUnits;
        logger.info(`Current capacity of table '${tableName}': read=${currentRead}, write=${currentWrite}.`);

        // determine if to adjust
        const capacityKeyName = tableName;
        if ((!_.isInteger(read) || read === currentRead) && (!_.isInteger(write) || write === currentWrite)) {
          logger.info('Same capacity, no need to adjust.');

          if (restorable) {
            capacity[capacityKeyName] = {
              read: null,
              write: null
            };
          }

          return;
        }

        // keep track of original capacity
        capacity[capacityKeyName] = {
          read: currentRead,
          write: currentWrite
        };

        read = _.isInteger(read) && read >= 1 ? read : currentRead;
        write = _.isInteger(write) && write >= 1 ? write : currentWrite;

        // adjust capacity
        yield adjustCapacity(tableName, read, write);

        // wait for the table to be ready
        if (waitForDone) {
          yield this.waitFor(tableName);
        }
      }.bind(this)));
    }

    /**
     * Restore capacity of the specified table.
     * @static
     * @param   {String}          tableName                       The name of table.
     * @param   {Object}          [options]                       Options.
     * @param   {Boolean}         [options.waitForDone=true]      A boolean value indicating whether to wait for the creation finished.
     * @return  {Promise}                                         A Promise that will be resolved when the operations was done if waitForDone is true,
     *                                                            or resolved immediately when the DynamoDB.updateTable() was returned otherwise.
     * @memberof DynamoDBUtils
     */
    static restoreCapacity(tableName, { waitForDone = true } = {}) {
      return Promise.resolve(co(function* handler() {
        // check parameters
        if (!_.isString(tableName)) {
          throw new Error('Invalid parameters.');
        }

        const capacityKeyName = tableName;

        // check prerequisite
        if (!_.isPlainObject(capacity) || !_.isPlainObject(capacity[capacityKeyName])) {
          throw new Error(`There is no saved original capacity for '${capacityKeyName}'.`);
        } else if (_.isNull(capacity[capacityKeyName].read) && _.isNull(capacity[capacityKeyName].write)) {
          logger.info('Same capacity, no need to restore.');
          return;
        }

        // wait for the table to be ready
        yield this.waitFor(tableName);

        // adjust capacity
        yield adjustCapacity(tableName, capacity[capacityKeyName].read, capacity[capacityKeyName].write);

        // delete the capacity cache
        delete capacity[capacityKeyName];

        // wait for the table to be ready
        if (waitForDone) {
          yield this.waitFor(tableName);
        }
      }.bind(this)));
    }

    /**
     * Adjust capacity of the specified index of the specified table.
     * @static
     * @param   {String}          tableName                       The name of table.
     * @param   {String}          indexName                       The index name.
     * @param   {Number}          read                            The read capacity.
     * @param   {Number}          write                           The write capacity.
     * @param   {Object}          [options]                       Options.
     * @param   {Boolean}         [options.restorable=true]       A boolean value indicating whether the adjustment is restorable.
     * @param   {Boolean}         [options.waitForDone=true]      A boolean value indicating whether to wait for the creation finished.
     * @return  {Promise}                                         A Promise that will be resolved when the operations was done if waitForDone is true,
     *                                                            or resolved immediately when the DynamoDB.updateTable() was returned otherwise.
     * @memberof DynamoDBUtils
     */
    static adjustIndexCapacity(tableName, indexName, read, write, { restorable = true, waitForDone = true } = {}) {
      return Promise.resolve(co(function* handler() {
        // check parameters
        if (!_.isString(tableName) || !_.isString(indexName) || ((!_.isInteger(read) || read < 1) && (!_.isInteger(write) || write < 1))) {
          throw new Error('Invalid parameters.');
        }

        // wait for the table to be ready
        yield this.waitFor(tableName);

        // get table info
        const describeTableParams = {
          TableName: tableName
        };
        const describeTableResult = yield dynamoDB.describeTable(describeTableParams).promise();
        logger.debug(`describeTable() result:\r\n${stringify(describeTableResult)}`);

        // get provisioned throughput
        const globalSecondaryIndexes = _.get(describeTableResult, 'Table.GlobalSecondaryIndexes');
        if (!_.isArray(globalSecondaryIndexes)) {
          logger.error(`Invalid result:\r\n${stringify(describeTableResult)}`);
          throw new Error('Invalid result.');
        }

        // index global secondary indexes by IndexName
        const globalIndex = _.keyBy(globalSecondaryIndexes, 'IndexName');
        if (!_.has(globalIndex, indexName)) {
          logger.error(`There is no such index '${indexName}'.`);
          throw new Error(`There is no such index '${indexName}'.`);
        }

        // get current read/write
        const currentRead = _.get(globalIndex[indexName], 'ProvisionedThroughput.ReadCapacityUnits');
        const currentWrite = _.get(globalIndex[indexName], 'ProvisionedThroughput.WriteCapacityUnits');
        logger.info(`Current capacity of index '${indexName}' on table '${tableName}': read=${currentRead}, write=${currentWrite}.`);

        // determine if to adjust
        const capacityKeyName = `${tableName},${indexName}`;
        if ((!_.isInteger(read) || read === currentRead) && (!_.isInteger(write) || write === currentWrite)) {
          logger.info('Same capacity, no need to adjust.');

          if (restorable) {
            capacity[capacityKeyName] = {
              read: null,
              write: null
            };
          }

          return;
        }

        // keep track of original capacity
        if (restorable) {
          capacity[capacityKeyName] = {
            read: currentRead,
            write: currentWrite
          };
        }

        read = _.isInteger(read) && read >= 1 ? read : currentRead;
        write = _.isInteger(write) && write >= 1 ? write : currentWrite;

        // adjust capacity
        yield adjustIndexCapacity(tableName, indexName, read, write);

        // wait for the table to be ready
        if (waitForDone) {
          yield this.waitFor(tableName);
        }
      }.bind(this)));
    }

    /**
     * Restore capacity of the specified index of the specified table.
     * @static
     * @param   {String}          tableName                       The name of table.
     * @param   {String}          indexName                       The index name.
     * @param   {Object}          [options]                       Options.
     * @param   {Boolean}         [options.waitForDone=true]      A boolean value indicating whether to wait for the creation finished.
     * @return  {Promise}                                         A Promise that will be resolved when the operations was done if waitForDone is true,
     *                                                            or resolved immediately when the DynamoDB.updateTable() was returned otherwise.
     * @memberof DynamoDBUtils
     */
    static restoreIndexCapacity(tableName, indexName, { waitForDone = true } = {}) {
      return Promise.resolve(co(function* handler() {
        // check parameters
        if (!_.isString(tableName) || !_.isString(indexName)) {
          throw new Error('Invalid parameters.');
        }

        const capacityKeyName = `${tableName},${indexName}`;

        // check prerequisite
        if (!_.isPlainObject(capacity) || !_.isPlainObject(capacity[capacityKeyName])) {
          throw new Error(`There is no saved original capacity for '${capacityKeyName}'.`);
        } else if (_.isNull(capacity[capacityKeyName].read) && _.isNull(capacity[capacityKeyName].write)) {
          logger.info('Same capacity, no need to restore.');
          return; // no need to update
        }

        // wait for the table to be ready
        yield this.waitFor(tableName);

        // adjust capacity
        yield adjustIndexCapacity(tableName, indexName, capacity[capacityKeyName].read, capacity[capacityKeyName].write);

        // delete the capacity cache
        delete capacity[capacityKeyName];

        // wait for the table to be ready
        if (waitForDone) {
          yield this.waitFor(tableName);
        }
      }.bind(this)));
    }

    /**
     * Scan all items of the specified table.
     * @static
     * @param   {String}          tableName                       The name of table.
     * @param   {Object}          [params]                        The params that is passed to DynamoDB.scan(),
     *                                                            see here: http://docs.aws.amazon.com/AWSJavaScriptSDK/latest/AWS/DynamoDB/DocumentClient.html#scan-property
     * @param   {Object}          [options]                       Options.
     * @param   {ScanCallback}    [options.callback]              Callback function.
     *                                                            If specified, the callback function will be called for every batch pulled from the database along with
     *                                                            items and next key. In this case, the returned Promise will be resolved with the number of total items being scanned.
     * @param   {Object}          [options.lastEvaluatedKey]      The last evaluated key.
     * @param   {String[]}        [options.selectAttributes]      Array of attributes to select, return all if not specified.
     * @return  {Promise<Object[]>|Promise<Number>}               A Promise that resolves the total amount of items or the array of items depends on the presence of callback.
     * @memberof DynamoDBUtils
     */
    static scanAll(tableName, params, { callback = null, lastEvaluatedKey, selectAttributes } = {}) {
      return Promise.resolve(co(function* handler() {
        // check parameters
        if (!_.isString(tableName) || _.isEmpty(tableName)) {
          throw new Error('Invalid table name.');
        } else if (!_.isNil(params) && !_.isPlainObject(params)) {
          throw new Error('Invalid params.');
        } else if (!_.isNil(callback) && !_.isFunction(callback)) {
          throw new Error('Invalid callback.');
        } else if (!_.isNil(lastEvaluatedKey) && !_.isPlainObject(lastEvaluatedKey)) {
          throw new Error('Invalid last evaluated key.');
        }

        params = _.extend({
          TableName: tableName,
        }, params, buildParams('scan', null, { selectAttributes }));

        const { nextKey, items } = yield scan(params, lastEvaluatedKey);
        logger.debug(`${items.length} items are returned.`);
        if (_.isFunction(callback)) {
          yield callback({
            items,
            nextKey,
            done: _.isNil(nextKey)
          });
        }

        let result;
        if (nextKey) {
          result = yield this.scanAll(tableName, params, {
            callback,
            lastEvaluatedKey: nextKey,
            selectAttributes
          });
        }

        if (!_.isFunction(callback)) {
          return result ? _.concat(items, result) : items;
        }

        return _.isInteger(result) ? result + items.length : items.length;
      }.bind(this)));
    }

    /**
     * Scan items of the specified table.
     * @static
     * @param   {String}          tableName                       The name of table.
     * @param   {Object}          params                          The params that is passed to DynamoDB.scan(),
     *                                                            see here: http://docs.aws.amazon.com/AWSJavaScriptSDK/latest/AWS/DynamoDB/DocumentClient.html#scan-property
     * @param   {Object}          [options]                       Options.
     * @param   {Object}          [options.lastEvaluatedKey]      The last evaluated key.
     * @param   {Number}          [options.limit]                 The limit.
     * @param   {String[]}        [options.selectAttributes]      Array of attributes to select, return all if not specified.
     * @return  {Promise<SQResult>}                               A Promise that resolves the result.
     * @memberof DynamoDBUtils
     */
    static scan(tableName, params, { lastEvaluatedKey, limit, selectAttributes } = {}) {
      return Promise.resolve(co(function* handler() {
        // check parameters
        if (!_.isString(tableName) || _.isEmpty(tableName)) {
          throw new Error('Invalid table name.');
        } else if (!_.isNil(params) && !_.isPlainObject(params)) {
          throw new Error('Invalid params.');
        } else if (!_.isNil(lastEvaluatedKey) && !_.isPlainObject(lastEvaluatedKey)) {
          throw new Error('Invalid last evaluated key.');
        } else if (!_.isNil(limit) && !_.isInteger(limit)) {
          throw new Error('Invalid limit.');
        }

        params = _.extend({
          TableName: tableName,
          Limit: limit
        }, params, buildParams('scan', null, { selectAttributes }));

        const { nextKey, items } = yield scan(params, lastEvaluatedKey, { selectAttributes });

        return {
          items,
          nextKey,
          done: _.isNil(nextKey)
        };
      }));
    }

    /**
     * Query all items of the specified table.
     * @static
     * @param   {String}          tableName                       The name of table.
     * @param   {Object}          key                             The key to query.
     * @param   {Object}          [options]                       Options.
     * @param   {String}          [options.indexName]             The index name.
     * @param   {ScanCallback}    [options.callback]              Callback function.
     *                                                            If specified, the callback function will be called for every batch pulled from the database along with
     *                                                            items and next key. In this case, the returned Promise will be resolved with the number of total items being scanned.
     * @param   {Object}          [options.lastEvaluatedKey]      The last evaluated key.
     * @param   {String[]}        [options.selectAttributes]      Array of attributes to select, return all if not specified.
     * @return  {Promise<Object[]>|Promise<Number>}               A Promise that resolves the total amount of items or the array of items depends on the presence of callback.
     * @memberof DynamoDBUtils
     */
    static queryAll(tableName, key, { indexName, callback = null, lastEvaluatedKey, selectAttributes } = {}) {
      return Promise.resolve(co(function* handler() {
        // check parameters
        if (!_.isString(tableName) || _.isEmpty(tableName)) {
          throw new Error('Invalid table name.');
        } else if (!_.isPlainObject(key)) {
          throw new Error('Invalid key.');
        } else if (!_.isNil(indexName) && !_.isString(indexName)) {
          throw new Error('Invalid index name.');
        } else if (!_.isNil(callback) && !_.isFunction(callback)) {
          throw new Error('Invalid callback.');
        } else if (!_.isNil(lastEvaluatedKey) && !_.isPlainObject(lastEvaluatedKey)) {
          throw new Error('Invalid last evaluated key.');
        }

        const params = _.extend({
          TableName: tableName,
          IndexName: indexName,
        }, buildParams('query', key, { selectAttributes }));

        const { nextKey, items } = yield query(params, lastEvaluatedKey);
        if (_.isFunction(callback)) {
          yield callback({
            items,
            nextKey,
            done: _.isNil(nextKey)
          });
        }

        let result;
        if (nextKey) {
          result = yield this.queryAll(tableName, params, {
            callback,
            lastEvaluatedKey: nextKey,
            selectAttributes
          });
        }

        if (!_.isFunction(callback)) {
          return result ? _.concat(items, result) : items;
        }

        return _.isInteger(result) ? result + items.length : items.length;
      }.bind(this)));
    }

    /**
     * Query items of the specified table.
     * @static
     * @param   {String}          tableName                       The name of table.
     * @param   {Object}          key                             The key to query.
     * @param   {Object}          [options]                       Options.
     * @param   {Object}          [options.lastEvaluatedKey]      The last evaluated key.
     * @param   {Number}          [options.limit]                 The limit.
     * @param   {String[]}        [options.selectAttributes]      Array of attributes to select, return all if not specified.
     * @return  {Promise<SQResult>}                               A Promise that resolves the result.
     * @memberof DynamoDBUtils
     */
    static query(tableName, key, { indexName, lastEvaluatedKey, limit, selectAttributes } = {}) {
      return Promise.resolve(co(function* handler() {
        // check parameters
        if (!_.isString(tableName) || _.isEmpty(tableName)) {
          throw new Error('Invalid table name.');
        } else if (!_.isPlainObject(key)) {
          throw new Error('Invalid key.');
        } else if (!_.isNil(indexName) && !_.isString(indexName)) {
          throw new Error('Invalid index name.');
        } else if (!_.isNil(lastEvaluatedKey) && !_.isPlainObject(lastEvaluatedKey)) {
          throw new Error('Invalid last evaluated key.');
        } else if (!_.isNil(limit) && !_.isInteger(limit)) {
          throw new Error('Invalid limit.');
        }

        const params = _.extend({
          TableName: tableName,
          IndexName: indexName,
          Limit: limit,
        }, buildParams('query', key, { selectAttributes }));

        const { nextKey, items } = yield query(params, lastEvaluatedKey);

        return {
          items,
          nextKey,
          done: _.isNil(nextKey)
        };
      }));
    }

    /**
     * Get specific item from the specified table.
     * @static
     * @param   {String}          tableName                       The name of table.
     * @param   {Object}          key                             The key to get.
     * @return  {Promise<Object>}                                 A Promise that resolves the retrieved item.
     * @memberof DynamoDBUtils
     */
    static get(tableName, key) {
      return Promise.resolve(co(function* handler() {
        // check parameters
        if (!_.isString(tableName) || _.isEmpty(tableName)) {
          throw new Error('Invalid table name.');
        } else if (!_.isPlainObject(key)) {
          throw new Error('Invalid key.');
        }

        const params = {
          TableName: tableName,
          Key: key
        };

        const result = yield docClient.get(params).promise();
        return _.get(result, 'Item', null);
      }));
    }

    /**
     * Put specific item into the specified table.
     * @static
     * @param   {String}          tableName                       The name of table.
     * @param   {Object}          item                            The item to put.
     * @param   {Object}          [options]                       Options.
     * @param   {String[]}        [options.existingAttributes]    A condition in array of attributes that should be existed.
     * @param   {String[]}        [options.nonExistingAttributes] A condition in array of attributes that should not be existed.
     * @param   {Object|Condition[]} [options.conditions]         Conditions.
     * @return  {Promise<Object>}                                 A Promise that resolves the item being put.
     * @memberof DynamoDBUtils
     */
    static put(tableName, item, { existingAttributes, nonExistingAttributes, conditions } = {}) {
      return Promise.resolve(co(function* handler() {
        // check parameters
        if (!_.isString(tableName) || _.isEmpty(tableName)) {
          throw new Error('Invalid table name.');
        } else if (!_.isPlainObject(item)) {
          throw new Error('Invalid item.');
        } else if (!_.isNil(existingAttributes) && !_.isArray(existingAttributes)) {
          throw new Error('Invalid existing attributes.');
        } else if (!_.isNil(nonExistingAttributes) && !_.isArray(nonExistingAttributes)) {
          throw new Error('Invalid non-existing attributes.');
        } else if (!_.isNil(conditions) && !_.isArray(conditions) && !_.isPlainObject(conditions)) {
          throw new Error('Invalid conditions.');
        }

        const params = _.extend({
          TableName: tableName,
          Item: omitEmptyProperties(item),
        }, buildParams('put', null, { existingAttributes, nonExistingAttributes, conditions }));

        yield docClient.put(params).promise();
        return item;
      }));
    }

    /**
     * Update specific item in the specified table.
     * @static
     * @param   {String}            tableName                     The name of table.
     * @param   {Object}            key                           The key to update.
     * @param   {Object}            updatedData                   The updated data.
     * @param   {Object|Condition[]} conditions                   Conditions.
     *                                                            - If object is specified, all the values must be matched with equality.
     *                                                            - If array of conditions is specified, a DynamoDB conditional expressions will be performed.
     * @return  {Promise<Object>}                                 A Promise that resolves the updated item.
     * @memberof DynamoDBUtils
     */
    static update(tableName, key, updatedData, conditions) {
      return Promise.resolve(co(function* handler() {
        // check parameters
        if (!_.isString(tableName) || _.isEmpty(tableName)) {
          throw new Error('Invalid table name.');
        } else if (!_.isPlainObject(key)) {
          throw new Error('Invalid key.');
        } else if (!_.isPlainObject(updatedData)) {
          throw new Error('Invalid data to update.');
        } else if (!_.isNil(conditions) && !_.isArray(conditions) && !_.isPlainObject(conditions)) {
          throw new Error('Invalid conditions.');
        }

        const params = _.extend({
          TableName: tableName,
          Key: key,
          ReturnValues: 'ALL_NEW'
        }, buildParams('update', updatedData, { existingAttributes: _.keys(key), conditions }));

        const result = yield docClient.update(params).promise();
        return _.get(result, 'Attributes');
      }));
    }

    /**
     * Delete specific item from the specified table.
     * @static
     * @param   {String}            tableName                     The name of table.
     * @param   {Object}            key                           The key to delete.
     * @return  {Promise}                                         A Promise that resolves when the operation is finished.
     * @todo    The results should be simplified.
     * @memberof DynamoDBUtils
     */
    static delete(tableName, key) {
      return Promise.resolve(co(function* handler() {
        // check parameters
        if (!_.isString(tableName) || _.isEmpty(tableName)) {
          throw new Error('Invalid table name.');
        } else if (!_.isPlainObject(key)) {
          throw new Error('Invalid key.');
        }

        const params = {
          TableName: tableName,
          Key: key
        };

        yield docClient.delete(params).promise();
      }));
    }

    /**
     * Batch get items from the specified table.
     * @static
     * @param   {String}            tableName                     The name of table.
     * @param   {Object[]}          keys                          Array of keys.
     * @return  {Promise<Object[]>}                               A Promise that resolves an array of retrieved items.
     * @todo    The results should be simplified.
     * @memberof DynamoDBUtils
     */
    static batchGet(tableName, keys) {
      const keyBuckets = _.chunk(keys, 100);
      const totalCount = keys.length;

      return Promise.mapSeries(keyBuckets, (keys, index) => {
        logger.debug(`Getting ${index * 100} - ${(index + 1) * 100} (total: ${totalCount}) items...`);
        return Promise.resolve(co(batchGet(tableName, keys))).delay(200);
      }).then(_.flatten);
    }

    /**
     * Batch write items into the specified table.
     * @static
     * @param   {String}            tableName                     The name of table.
     * @param   {Object[]}          [putItems=[]]                 Array of items to put.
     * @param   {Object[]}          [deleteKeys=[]]               Array of keys to delete.
     * @return  {Promise}                                         A Promise that will be resolved when the operation is finished.
     * @memberof DynamoDBUtils
     */
    static batchWrite(tableName, putItems = [], deleteKeys = []) {
      const itemBuckets = _
        .chain([])
        .concat(_.map(putItems, item => ({ type: 'put', item })))
        .concat(_.map(deleteKeys, key => ({ type: 'delete', key })))
        .chunk(25)
        .value();
      const totalCount = putItems.length + deleteKeys.length;

      return Promise.mapSeries(itemBuckets, (items, index) => {
        const putItems = _.map(_.filter(items, { type: 'put' }), 'item');
        const deleteKeys = _.map(_.filter(items, { type: 'delete' }), 'key');

        logger.debug(`Writing ${index * 25} - ${(index + 1) * 25} (put: ${putItems.length}, delete: ${deleteKeys.length}, total: ${totalCount}) items...`);
        return Promise.resolve(co(batchWriteItems(tableName, putItems, deleteKeys))).delay(200);
      }).then(() => Promise.resolve());
    }

    /**
     * Batch put items into the specified table.
     * @static
     * @param   {String}            tableName                     The name of table.
     * @param   {Object[]}          items                         Array of items to put.
     * @return  {Promise<Object[]>}                               A Promise that resolves the array of items being put.
     * @memberof DynamoDBUtils
     */
    static batchPut(tableName, items) {
      const itemBuckets = _.chunk(items, 25);
      const totalCount = items.length;

      return Promise.mapSeries(itemBuckets, (items, index) => {
        logger.debug(`Putting ${index * 25} - ${(index + 1) * 25} (total: ${totalCount}) items...`);
        return Promise.resolve(co(batchWriteItems(tableName, items, []))).delay(200);
      }).then(_.constant(items));
    }

    /**
     * Batch delete items from the specified table.
     * @static
     * @param   {String}            tableName                     The name of table.
     * @param   {Object[]}          keys                          Array of keys to delete.
     * @return  {Promise<Object[]>}                               A Promise that resolves the array of keys being deleted.
     * @memberof DynamoDBUtils
     */
    static batchDelete(tableName, keys) {
      const keyBuckets = _.chunk(keys, 25);
      const totalCount = keys.length;

      return Promise.mapSeries(keyBuckets, (keys, index) => {
        logger.debug(`Deleting ${index * 25} - ${(index + 1) * 25} (total: ${totalCount}) items...`);
        return Promise.resolve(co(batchWriteItems(tableName, [], keys))).delay(200);
      }).then(_.constant(keys));
    }

    /**
     * Truncate the specified table.
     * @static
     * @param   {String}            tableName                     The name of table.
     * @return  {Promise<Object[]>}                               A Promise that resolves the array of keys being deleted.
     * @memberof DynamoDBUtils
     */
    static truncate(tableName) {
      return Promise.resolve(co(function* handler() {
        // check parameters
        if (!_.isString(tableName) || _.isEmpty(tableName)) {
          throw new Error('Invalid table name.');
        }

        // get table info
        const describeTableParams = {
          TableName: tableName
        };
        const describeTableResult = yield dynamoDB.describeTable(describeTableParams).promise();
        logger.debug(`describeTable() result:\r\n${stringify(describeTableResult)}`);

        // get key schema
        const keySchema = _.get(describeTableResult, 'Table.KeySchema');
        if (!_.isArray(keySchema)) {
          logger.error(`Invalid result:\r\n${stringify(describeTableResult)}`);
          throw new Error('Invalid result.');
        }

        const keyNames = _.map(keySchema, 'AttributeName');

        yield this.scanAll(tableName, null, {
          callback: ({ items, nextKey, done }) => this.batchDelete(tableName, _.map(items, item => _.pick(item, keyNames)))
        });
      }.bind(this)));
    }
  }

  // conditions will be like { a: 'test', b: 1234 } or [{ expression: '#b > :b', attributeNames: { '#b': 'b' }, attributeValues: { ':b': 1234 } }, ... ]
  function buildParams(type, keyValuePair, { existingAttributes, nonExistingAttributes, selectAttributes, conditions } = {}) {
    // build expression attribute names from keyValuePair
    const expressionAttributeNames = _
      .chain(keyValuePair || {})
      .mapValues((value, key) => key)
      .mapKeys((value, key) => _.camelCase(key))
      .value();

    // build expression attribute values from expressionAttributeNames
    const expressionAttributeValues = _.mapValues(expressionAttributeNames, originalKey => keyValuePair[originalKey]);

    // add existingAttributes & nonExistingAttributes
    const conditionExpressions = [];
    const projectionExpressions = [];
    _.each(existingAttributes || [], (existingAttribute) => {
      const camelCase = _.camelCase(existingAttribute);
      expressionAttributeNames[camelCase] = existingAttribute;
      conditionExpressions.push(`attribute_exists(#${camelCase})`);
    });
    _.each(nonExistingAttributes || [], (existingAttribute) => {
      const camelCase = _.camelCase(existingAttribute);
      expressionAttributeNames[camelCase] = existingAttribute;
      conditionExpressions.push(`attribute_not_exists(#${camelCase})`);
    });
    _.each(selectAttributes || [], (selectAttribute) => {
      const camelCase = _.camelCase(selectAttribute);
      expressionAttributeNames[camelCase] = selectAttribute;
      projectionExpressions.push(camelCase);
    });

    // add other condition expressions
    if (_.isPlainObject(conditions)) {
      _.forIn(conditions, (value, key) => {
        const camelCase = _.camelCase(key);
        expressionAttributeNames[camelCase] = key;
        expressionAttributeValues[camelCase] = value;
        conditionExpressions.push(`#${camelCase} = :${camelCase}`);
      });
    } else if (!_.isArray(conditions)) {
      _.each(conditions, (condition) => {
        const { expression, attributeNames = {}, attributeValues = {} } = condition;
        _.extend(expressionAttributeNames, attributeNames);
        _.extend(expressionAttributeValues, attributeValues);
        conditionExpressions.push(expression);
      });
    }

    // build ret
    const ret = {};
    if (!_.isEmpty(expressionAttributeNames)) {
      ret.ExpressionAttributeNames = _.mapKeys(expressionAttributeNames, (originalKey, camelCaseKey) => `#${camelCaseKey}`);
    }
    if (!_.isEmpty(expressionAttributeValues)) {
      ret.ExpressionAttributeValues = _
        .chain(expressionAttributeValues)
        .omitBy((value, key) => _.isNil(value) || (_.isString(value) && _.isEmpty(value)))
        .mapKeys((originalValue, camelCaseKey) => `:${camelCaseKey}`)
        .value();
    }
    if (conditionExpressions.length > 0) {
      ret.ConditionExpression = conditionExpressions.join(' AND ');
    }
    if (!_.isEmpty(projectionExpressions)) {
      ret.ProjectionExpression = _.join(_.map(projectionExpressions, projectionExpression => `#${projectionExpression}`), ', ');
    }

    if (type === 'update') {
      const setExpressions = _
        .chain(expressionAttributeValues)
        .omitBy((value, key) => _.isNil(value) || (_.isString(value) && _.isEmpty(value)))
        .keys()
        .map(camelCaseKey => `#${camelCaseKey} = :${camelCaseKey}`)
        .value();

      const removeExpressions = _
        .chain(expressionAttributeValues)
        .pickBy((value, key) => _.isNil(value) || (_.isString(value) && _.isEmpty(value)))
        .keys()
        .map(camelCaseKey => `#${camelCaseKey}`)
        .value();

      ret.UpdateExpression = '';
      if (setExpressions.length > 0) {
        ret.UpdateExpression += ` SET ${setExpressions.join(', ')}`;
      }
      if (removeExpressions.length > 0) {
        ret.UpdateExpression += ` REMOVE ${removeExpressions.join(', ')}`;
      }

      ret.UpdateExpression = _.trim(ret.UpdateExpression);
    } else if (type === 'query') {
      const expressions = _.map(_.keys(expressionAttributeValues), camelCaseKey => `#${camelCaseKey} = :${camelCaseKey}`);
      ret.KeyConditionExpression = expressions.join(' AND ');
    }

    return ret;
  }

  /* eslint-disable id-length */
  function stringify(s) {
    return JSON.stringify(s, null, 2);
  }
  /* eslint-enable id-length */

  function omitEmptyProperties(object, { omitEmptyObject = true, omitEmptyArray = false, removeNullElementInArray = false } = {}) {
    if (_.isArray(object)) {
      object = _.map(object, omitEmptyProperties);
      if (removeNullElementInArray) {
        object = _.compact(object);
      }
      if (omitEmptyArray && _.isEmpty(omitEmptyArray)) {
        object = null;
      }
    } else if (_.isPlainObject(object)) {
      object = _
        .chain(object)
        .mapValues(omitEmptyProperties)
        .omitBy(value => _.isNil(value)
          || (_.isString(value) && _.isEmpty(value))
          || (omitEmptyObject === true && _.isPlainObject(value) && _.isEmpty(value))
        )
        .value();
    }

    return object;
  }

  function* adjustCapacity(tableName, read, write) {
    // check parameters
    if (!_.isString(tableName) || !_.isInteger(read) || !_.isInteger(write) || read < 1 || write < 1) {
      logger.error(`Invalid parameters:\r\ntableName: ${stringify(tableName)}\r\nread: ${stringify(read)}\r\nwrite: ${stringify(write)}`);
      throw new Error('Invalid parameters.');
    }

    const updateTableParams = {
      TableName: tableName,
      ProvisionedThroughput: {
        ReadCapacityUnits: read,
        WriteCapacityUnits: write
      }
    };

    logger.info(`Adjusting capacities of table '${tableName}' to read=${read}, write=${write}...`);

    yield dynamoDB.updateTable(updateTableParams).promise();
  }

  function* adjustIndexCapacity(tableName, indexName, read, write) {
    // check parameters
    if (!_.isString(tableName) || !_.isString(indexName) || !_.isInteger(read) || !_.isInteger(write) || read < 1 || write < 1) {
      logger.error(`Invalid parameters:\r\ntableName: ${stringify(tableName)}\r\nindexName: ${stringify(indexName)}\r\nread: ${stringify(read)}\r\nwrite: ${stringify(write)}`);
      throw new Error('Invalid parameters.');
    }

    const updateTableParams = {
      TableName: tableName,
      GlobalSecondaryIndexUpdates: [{
        Update: {
          IndexName: indexName,
          ProvisionedThroughput: {
            ReadCapacityUnits: read,
            WriteCapacityUnits: write
          }
        }
      }]
    };

    logger.debug(`Adjusting capacities of index '${indexName}' on table '${tableName}' to read=${read}, write=${write}...`);

    yield dynamoDB.updateTable(updateTableParams).promise();
  }

  function* batchGet(tableName, keys) {
    // check parameters
    if (!_.isString(tableName) || !_.isArray(keys)) {
      logger.error(`Invalid parameters.\r\ntableName: ${stringify(tableName)}\r\nkeys: ${stringify(keys)}`);
      throw new Error('Invalid parameters.');
    }

    const params = {
      RequestItems: {
        [tableName]: {
          Keys: keys
        }
      }
    };

    try {
      const result = yield docClient.batchGet(params).promise();
      if (!_.isObject(result) || !_.isObject(result.UnprocessedKeys) || !_.isObject(result.Responses)) {
        logger.error(`Invalid result:\r\n${stringify(result)}`);
        throw new Error('Invalid result.');
      }

      const items = _.get(result, ['Responses', tableName]);

      if (!_.isEmpty(result.UnprocessedKeys)) {
        logger.debug(`There are still ${result.UnprocessedItems[tableName].length} unprocessed items...`);
        return _.concat(items, yield batchGet(tableName, result.UnprocessedKeys));
      }

      return items;
    } catch (error) {
      logger.error('batchGet() error:', error);
      logger.warn(`params:\r\n${stringify(params)}`);
      throw error;
    }
  }

  function* batchWrite(tableName, requestItems) {
    const params = {
      RequestItems: requestItems
    };

    try {
      const result = yield docClient.batchWrite(params).promise();
      if (!_.isObject(result) || !_.isObject(result.UnprocessedItems)) {
        logger.error(`Invalid result:\r\n${stringify(result)}`);
        throw new Error('Invalid result.');
      }

      if (!_.isEmpty(result.UnprocessedItems)) {
        logger.debug(`There are still ${result.UnprocessedItems[tableName].length} unprocessed items...`);
        yield batchWrite(tableName, result.UnprocessedItems);
      }
    } catch (error) {
      logger.error('batchWrite() error:', error);
      logger.warn(`params:\r\n${stringify(params)}`);
      throw error;
    }
  }

  function* batchWriteItems(tableName, putItems = [], deleteKeys = []) {
    // check parameters
    if (!_.isString(tableName) || !_.isArray(putItems) || !_.isArray(deleteKeys)) {
      logger.error(`Invalid parameters:\r\ntableName: ${stringify(tableName)}\r\nputItems: ${stringify(putItems)}\r\ndeleteKeys: ${stringify(deleteKeys)}`);
      throw new Error('Invalid parameters.');
    }

    logger.debug(`Writing ${putItems.length} items and deleting ${deleteKeys.length} items...`);

    const requestItems = {
      [tableName]: _.concat(
        _.map(putItems, item => ({
          PutRequest: {
            Item: omitEmptyProperties(item)
          }
        })),
        _.map(deleteKeys, key => ({
          DeleteRequest: {
            Key: key
          }
        }))
      )
    };

    return yield batchWrite(tableName, requestItems);
  }

  function* scan(params, exclusiveStartKey) {
    logger.debug('params:', params);
    logger.debug('exclusiveStartKey:', exclusiveStartKey);

    params = exclusiveStartKey ? _.extend(params, {
      ExclusiveStartKey: exclusiveStartKey
    }) : params;

    try {
      const result = yield docClient.scan(params).promise();
      if (!_.isPlainObject(result) || !_.isArray(result.Items)) {
        logger.error(`Invalid result:\r\n${stringify(result)}`);
        throw new Error('Invalid result.');
      }

      return {
        nextKey: result.LastEvaluatedKey,
        items: result.Items
      };
    } catch (error) {
      logger.error('scan() error:', error);
      logger.warn(`params:\r\n${stringify(params)}`);
      throw error;
    }
  }

  function* query(params, exclusiveStartKey) {
    logger.debug('exclusiveStartKey:', exclusiveStartKey);

    params = exclusiveStartKey ? _.extend(params, {
      ExclusiveStartKey: exclusiveStartKey
    }) : params;

    try {
      const result = yield docClient.query(params).promise();
      if (!_.isPlainObject(result) || !_.isArray(result.Items)) {
        logger.error(`Invalid result:\r\n${stringify(result)}`);
        throw new Error('Invalid result.');
      }

      return {
        nextKey: result.LastEvaluatedKey,
        items: result.Items
      };
    } catch (error) {
      logger.error('query() error:', error);
      logger.warn(`params:\r\n${stringify(params)}`);
      throw error;
    }
  }

  function isToUpdateTable(describeTableResult, createTableParams) {
    const tableParams = describeTableResult.Table;

    const ret = {
      provisionedThroughput: isSameProvisionedThroughput(createTableParams.ProvisionedThroughput, tableParams.ProvisionedThroughput),
      globalSecondaryIndexes: isSameGlobalSecondaryIndexes(createTableParams.GlobalSecondaryIndexes, tableParams.GlobalSecondaryIndexes),
      streamSpecification: isSameStreamSpecification(createTableParams.StreamSpecification, tableParams.StreamSpecification),
    };

    return _.every(ret, value => value === true) ? false : _.keys(_.pickBy(ret, value => value === false));
  }

  function isSameProvisionedThroughput(value1, value2) {
    const properties = ['ReadCapacityUnits', 'WriteCapacityUnits'];

    function hashProvisionedThroughput(val) {
      return hash(_.pick(val, properties), { unorderedArrays: true });
    }

    return hashProvisionedThroughput(value1) === hashProvisionedThroughput(value2);
  }

  function isSameGlobalSecondaryIndexes(value1, value2) {
    const properties = ['IndexName', 'KeySchema', 'Projection', 'ProvisionedThroughput'];

    function hashGlobalSecondaryIndexes(val) {
      return hash(_.map(val, globalSecondaryIndex => _.extend(_.pick(globalSecondaryIndex, properties), {
        ProvisionedThroughput: _.pick(globalSecondaryIndex.ProvisionedThroughput, ['ReadCapacityUnits', 'WriteCapacityUnits'])
      })), { unorderedArrays: true });
    }

    return hashGlobalSecondaryIndexes(value1) === hashGlobalSecondaryIndexes(value2);
  }

  function isSameStreamSpecification(value1, value2) {
    const properties = ['StreamEnabled', 'StreamViewType'];

    function hashStreamSpecification(val) {
      return hash(_.pick(val, properties), { unorderedArrays: true });
    }

    return hashStreamSpecification(value1) === hashStreamSpecification(value2);
  }

  function buildUpdateTableParams(describeTableResult, createTableParams, attributes) {
    const currentGlobalSecondaryIndexes = _.keyBy(_.get(describeTableResult, ['Table', 'GlobalSecondaryIndexes'], []), 'IndexName');
    const newGlobalSecondaryIndexes = _.keyBy(_.get(createTableParams, 'GlobalSecondaryIndexes', []), 'IndexName');
    const createOperations = _.map(
      _.difference(
        _.keys(newGlobalSecondaryIndexes),
        _.keys(currentGlobalSecondaryIndexes)
      ),
      key => newGlobalSecondaryIndexes[key]
    );
    const updateOperations = _.map(
      _.filter(
        _.intersection(
          _.keys(newGlobalSecondaryIndexes),
          _.keys(currentGlobalSecondaryIndexes)
        ),
        key => !_.isEqual(
          _.pick(currentGlobalSecondaryIndexes[key].ProvisionedThroughput, ['ReadCapacityUnits', 'WriteCapacityUnits']),
          _.pick(newGlobalSecondaryIndexes[key].ProvisionedThroughput, ['ReadCapacityUnits', 'WriteCapacityUnits'])
        )
      ),
      key => _.pick(newGlobalSecondaryIndexes[key], ['IndexName', 'ProvisionedThroughput'])
    );
    const deleteOperations = _.map(
      _.difference(
        _.keys(currentGlobalSecondaryIndexes),
        _.keys(newGlobalSecondaryIndexes)
      ),
      key => ({ IndexName: key })
    );

    return _.omitBy({
      TableName: createTableParams.TableName,
      AttributeDefinitions: createTableParams.AttributeDefinitions,
      ProvisionedThroughput: _.includes(attributes, 'provisionedThroughput') ? createTableParams.ProvisionedThroughput : null,
      GlobalSecondaryIndexUpdates: _.includes(attributes, 'globalSecondaryIndexes') ? _.concat(
        _.map(createOperations, createOperation => ({
          Create: createOperation
        })),
        _.map(updateOperations, updateOperation => ({
          Update: updateOperation
        })),
        _.map(deleteOperations, deleteOperation => ({
          Delete: deleteOperation
        }))
      ) : [],
      StreamSpecification: _.includes(attributes, 'streamSpecification') ? createTableParams.ProvisionedThroughput : null,
    }, val => _.isNil(val) || (_.isArray(val) && _.isEmpty(val)));
  }

  return DynamoDBUtils;
};
